package com.example.foodtrack.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.airbnb.lottie.LottieAnimationView;
import com.example.foodtrack.R;

public class AddesSuccessfully extends AppCompatActivity {
Handler mHandler;
Runnable mRunnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addes_successfully);
        initview();

    }

    private void initview() {
        LottieAnimationView animationView = findViewById(R.id.animation_view);
        animationView.addAnimatorUpdateListener(
                (animation) -> {

                });
        animationView.playAnimation();

        if (animationView.isAnimating()) {

        }
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                finish();

            }
        };

        // its trigger runnable after 4000 millisecond.
        mHandler.postDelayed(mRunnable,3000);
    }
}
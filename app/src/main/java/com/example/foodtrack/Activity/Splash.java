package com.example.foodtrack.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.foodtrack.R;

public class Splash extends AppCompatActivity {
    Handler mHandler;
    Runnable mRunnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {

                    Intent splashIntent = new Intent(Splash.this, Splash2.class);
                    startActivity(splashIntent);

            }
        };

        // its trigger runnable after 3000 millisecond.
        mHandler.postDelayed(mRunnable,3000);
    }
}